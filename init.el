(require 'package)

(package-initialize)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/") t)

(add-to-list 'load-path "/home/hadjian/.emacs.d/custom/")
(require 'ox-rst)
(require 'ob-async)
(require 'ob-go)

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ediff-even-diff-A ((t (:background "color-22"))))
 '(ediff-odd-diff-A ((t (:background "color-24"))))
 '(ediff-odd-diff-B ((t (:background "color-237"))))
 '(flyspell-incorrect ((t (:inherit error :underline (:color foreground-color :style wave)))))
 '(font-lock-function-name-face ((t (:foreground "cyan"))))
 '(font-lock-variable-name-face ((t (:foreground "#D8DEE9"))))
 '(org-block ((t (:background "black" :foreground "#D8DEE9"))))
 '(org-code ((t (:background "black" :foreground "cyan"))))
 '(rst-level-1 ((t (:background "black" :foreground "white"))))
 '(rst-level-2 ((t (:background "grey78" :foreground "black" :weight bold))))
 '(rst-level-3 ((t (:background "grey71" :foreground "black" :weight bold))))
 '(smerge-base ((t (:background "color-135"))))
 '(smerge-lower ((t (:background "color-136"))))
 '(smerge-markers ((t (:background "color-33"))))
 '(smerge-refined-added ((t (:inherit smerge-refined-change :background "brightblack"))))
 '(smerge-refined-removed ((t (:inherit smerge-refined-change :background "brightblack"))))
 '(smerge-upper ((t (:background "brightred")))))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-idle-delay 0)
 '(custom-safe-themes
   (quote
    ("7f6d4aebcc44c264a64e714c3d9d1e903284305fd7e319e7cb73345a9994f5ef" "0fffa9669425ff140ff2ae8568c7719705ef33b7a927a0ba7c5e2ffcfac09b75" "13a8eaddb003fd0d561096e11e1a91b029d3c9d64554f8e897b2513dbf14b277" "51ec7bfa54adf5fff5d466248ea6431097f5a18224788d0bd7eb1257a4f7b773" "d6c5b8dc6049f2e9dabdfcafa9ef2079352640e80dffe3e6cc07c0f89cbf9748" "2809bcb77ad21312897b541134981282dc455ccd7c14d74cc333b6e549b824f3" "c433c87bd4b64b8ba9890e8ed64597ea0f8eb0396f4c9a9e01bd20a04d15d358" default)))
 '(debug-on-error nil)
 '(eglot-ignored-server-capabilites (quote (:documentSymbolProvider)))
 '(fill-column 72)
 '(git-link-remote-alist
   (quote
    (("git.sr.ht" git-link-sourcehut)
     ("github" git-link-github)
     ("bitbucket" git-link-bitbucket)
     ("gitorious" git-link-gitorious)
     ("code.siemens.com" git-link-gitlab)
     ("gitlab" git-link-gitlab)
     ("visualstudio\\|azure" git-link-azure))))
 '(go-test-verbose t)
 '(gofmt-command "goimports")
 '(indent-tabs-mode nil)
 '(ivy-mode t)
 '(line-number-mode nil)
 '(magit-refresh-verbose t)
 '(magit-repository-directories nil)
 '(menu-bar-mode nil)
 '(ob-http:curl-custom-arguments nil)
 '(olivetti-body-width 80)
 '(org-confirm-babel-evaluate nil)
 '(org-structure-template-alist
   (quote
    (("sh" "#+BEGIN_SRC sh :results output
?
#+END_SRC")
     ("h" "#+BEGIN_SRC http
?
#+END_SRC")
     ("s" "#+BEGIN_SRC ?

#+END_SRC")
     ("e" "#+BEGIN_EXAMPLE
?
#+END_EXAMPLE")
     ("q" "#+BEGIN_QUOTE
?
#+END_QUOTE")
     ("v" "#+BEGIN_VERSE
?
#+END_VERSE")
     ("V" "#+BEGIN_VERBATIM
?
#+END_VERBATIM")
     ("c" "#+BEGIN_CENTER
?
#+END_CENTER")
     ("C" "#+BEGIN_COMMENT
?
#+END_COMMENT")
     ("l" "#+BEGIN_EXPORT latex
?
#+END_EXPORT")
     ("L" "#+LaTeX: ")
     ("h" "#+BEGIN_EXPORT html
?
#+END_EXPORT")
     ("H" "#+HTML: ")
     ("a" "#+BEGIN_EXPORT ascii
?
#+END_EXPORT")
     ("A" "#+ASCII: ")
     ("i" "#+INDEX: ?")
     ("I" "#+INCLUDE: %file ?"))))
 '(package-selected-packages
   (quote
    (robots-txt-mode csharp-mode realgud ox-gfm git-link helm imenu-anywhere gitlab-ci-mode gitlab-ci-mode-flycheck ivy-gitlab solarized-theme forge flx-ido jdee go-dlv magit-todos yaml-mode go-projectile gotest flymake-go-staticcheck flymake-go markdown-preview-mode markdown-mode+ markdown-mode ace-window eglot ob-go ob-async ivy json-mode magit hydra go-mode go-eldoc virtualenvwrapper org htmlize ob-http olivetti gitlab flycheck yasnippet company projectile nord-theme)))
 '(select-enable-clipboard nil)
 '(select-enable-primary t)
 '(tab-width 4)
 '(winner-mode nil))

 ;; Modularized init.el with general settings in ./pedram/settings.el
(load "~/.emacs.d/custom/settings.el")
(load "~/.emacs.d/custom/golang.el")
(load "~/.emacs.d/custom/projectile-custom.el")
(load "~/.emacs.d/custom/org-custom.el")
(load "~/.emacs.d/custom/magit-custom.el")
(put 'narrow-to-region 'disabled nil)
