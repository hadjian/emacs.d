(define-package "magit" "20200927.1644" "A Git porcelain inside Emacs."
  '((emacs "25.1")
    (async "20200113")
    (dash "20200524")
    (git-commit "20200516")
    (transient "20200601")
    (with-editor "20200522"))
  :commit "6c1c92e5a2abbdf1c852f7da50e69f0b898e7a72" :keywords
  '("git" "tools" "vc"))
;; Local Variables:
;; no-byte-compile: t
;; End:
