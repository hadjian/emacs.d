;;; -*- no-byte-compile: t -*-
(define-package "go-rename" "20190805.2101" "Integration of the 'gorename' tool into Emacs." '((go-mode "1.3.1")) :commit "e10d6775f486ef09ed993567b0bd2c69f35deb45" :keywords '("tools"))
