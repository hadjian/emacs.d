;;; -*- no-byte-compile: t -*-
(define-package "flymake-go-staticcheck" "20190708.1325" "Go staticcheck linter for flymake" '((emacs "25")) :commit "130079fcd29c3e2a72f8325f3041042bcc6286f1" :keywords '("languages" "tools") :authors '(("Sergey Kostyaev" . "feo.me@ya.ru")) :maintainer '("Sergey Kostyaev" . "feo.me@ya.ru") :url "https://github.com/s-kostyaev/flymake-go-staticcheck")
