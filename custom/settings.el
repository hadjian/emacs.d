;; Theming
(load-theme 'nord t)

;; Olivetti
(require 'olivetti)

;; Defaults
(winner-mode)
(company-mode)
(ace-window-display-mode)
(global-auto-revert-mode)

;; Patched reStructuredText styling to go with nord theme
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(rst-level-1 ((t (:background "white" :foreground "black" :weight bold))))
 '(rst-level-2 ((t (:background "grey78" :foreground "black" :weight bold))))
 '(rst-level-3 ((t (:background "grey71" :foreground "black" :weight bold)))))

;; Centered text in most frequently used modes
(add-hook 'prog-mode-hook 'olivetti-mode)
(add-hook 'text-mode-hook 'olivetti-mode)
(add-hook 'magit-mode-hook 'olivetti-mode)

;; Define two functions, which set the view width to different numbers,
;; so I can activate them in different modes and change them
;; globally. E.g. magit looks nicer with 120 columns, but for coding I
;; prefer 80 columns.
(defun olivetti-wide () (olivetti-set-width 120))
(defun olivetti-narrow () (olivetti-set-width 80))


;; Custom Shortcuts
(global-set-key (kbd "C-c c i") 'helm-imenu)
(global-set-key (kbd "M-p") 'ace-window)
(global-set-key (kbd "C-c c f") 'find-file-at-point)
(global-set-key (kbd "C-c c r") 'revert-buffer)
(global-set-key (kbd "C-c c l") 'load-file)

