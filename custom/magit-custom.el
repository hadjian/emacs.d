(global-set-key (kbd "C-c m g") 'magit-status)
(global-set-key (kbd "C-c m b") 'magit-blame)
(global-set-key (kbd "C-c m d") 'magit-ediff)
(global-set-key (kbd "C-c m f") 'magit-diff-visit-worktree-file-other-window)

(add-hook 'magit-mode-hook 'olivetti-wide)
