;; Integrate go debugger delve
(require 'go-dlv)

(add-hook 'go-mode-hook 'company-mode)
(add-hook 'go-mode-hook 'eglot-ensure)
(add-hook 'go-mode-hook 'olivetti-narrow)
(add-hook 'go-mode-hook 'olivetti-mode)
(add-hook 'before-save-hook #'gofmt-before-save)

(require 'go-projectile)

;; Custom shortcuts for gotest
(define-key go-mode-map (kbd "C-c c f") 'go-test-current-file)
(define-key go-mode-map (kbd "C-c c t") 'go-test-current-test)
(define-key go-mode-map (kbd "C-c c p") 'go-test-current-project)
(define-key go-mode-map (kbd "C-c c b") 'go-test-current-benchmark)
(define-key go-mode-map (kbd "C-c c x") 'go-run)
