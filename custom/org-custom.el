;; My preferences for org-mode
(add-hook 'org-mode-hook 'olivetti-mode)

;; Languages used in my literate programming
(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (dot . t)
   (http . t)
   (shell . t)
   (plantuml . t)
   (C . t)))

(setq org-plantuml-jar-path
      (expand-file-name "/usr/share/plantuml/plantuml.jar"))

;; Add easy template for http source block.
;; In org files trigger with '<h <TAB>'
(add-to-list 'org-structure-template-alist
             '("h" "#+BEGIN_SRC http\n?\n#+END_SRC")
             '("pl" "#+BEGIN_SRC plantuml :file test.png\n?\n#+END_SRC"))
